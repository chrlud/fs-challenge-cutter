# FS Challenge Cutter
![Diagram](imgs/diagram_small.png)

[Julia](https://julialang.org/) notebook to cut the FS Challenge Image.

## Setup

### Install dependencies
In the julia console:

```julia
using Pkg
Pkg.add("Images")
Pkg.add("Interact")
Pkg.add("WebIO")
Pkg.add("IJulia")
Pkg.add("FileIO")
Pkg.add("IndirectArrays")
Pkg.add("OffsetArrays")
```

Follow instructions on
[https://juliagizmos.github.io/WebIO.jl/latest/providers/ijulia/](https://juliagizmos.github.io/WebIO.jl/latest/providers/ijulia/) to complete installation of `WebIO`.


### Start IJulia

```julia
using IJulia
notebook()
```
In the browser window navigate to the repository and open `cut_challenge.ipynb`
