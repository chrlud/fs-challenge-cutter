function trimUI(img)
    topw = spinbox([0:1000,],    label="Top"; value=0)
    bottomw = spinbox([0:1000,], label="Bottom"; value=0)
    leftw = spinbox([0:1000,],   label="Left"; value=0)
    rightw = spinbox([0:1000,],  label="Right"; value=0)

    ui = @manipulate for top in topw,
            bottom in bottomw,
            left in leftw,
            right in rightw
        cutimg = @view img[top+1:end-bottom,left+1:end-right]
    end
    
    ui, topw, bottomw, leftw, rightw
end

function erosionUI(distances)
    threshw = spinbox([1:100,], label="Erode Amount"; value=1)

    ui = @manipulate for thresh in threshw
        eroded_mask = map(px -> px >= thresh[], distances)
        Gray.(eroded_mask)
    end
    
    ui, threshw
end

function applyCutting(img, top, right, bottom, left, bg, offset = 3)
    img = img[top+1:end-bottom,left+1:end-right]
    padarray(img, Fill(bg, (offset,offset)))
end

# Define a distance metric on colors as the mean square difference on Red-Green-Blue color values
dist(c1::RGBA{Float32}, c2::RGBA{Float32}) = sqrt((red(c2)-red(c1))^2 + (green(c2)-green(c1))^2 + (blue(c2)-blue(c1))^2) / 3

function binarize(img::OffsetMatrix{RGBA{Float32}, Matrix{RGBA{Float32}}}, thresh::Float64, bg::RGBA{Float32})
    binary_mask = map(px -> dist(bg, px) < thresh, img)
    
    # Label the connected regions
    label_img = label_components(binary_mask)
    # Let the labels start from 1
    label_img .+= 1
    # Background label is at position [1,1], since we padded the image with bg color
    lbl_of_background = label_img[1,1]

    # Remove holes in the mask by nulling all pixels with labels not background
    for ix in eachindex(label_img)
        lbl = label_img[ix]
        if(lbl != lbl_of_background) 
            binary_mask[ix] = false
        end
    end

    # Invert the mask, so that background = false and foreground = true
    map(b -> !b, binary_mask)
end

function most_frequent_element_dict!(res::Vector{Int}, arr::Vector{Int}, d = Dict{Int,Int}())
    empty!(d)
    empty!(res)
    for ix in eachindex(arr)
        el = arr[ix]
        if (el <= 1) continue end
        d[el] = get(d, el, zero(el)) + 1
    end
    if isempty(d) return end
    maxVal = maximum(values(d))
    for (el, n) in d
        if n==maxVal push!(res, el) end
    end
end

function whisper_limit_area(mask, N)
    eightnbs =  Vector{CartesianIndex{2}}(vec(CartesianIndices((-1:1,-1:1))))
    deleteat!(eightnbs, 5)
    updates = Dict{Int32, Vector{CartesianIndex{2}}}()
    working_area = filter(lbl -> mask[lbl] > 0, CartesianIndices(mask))  # Limit the area to the foreground
    nb_labels = Vector{Int64}(undef, 8)
    mfe_dict = Dict{Int,Int}()
    update_lbls = Int[]

    for i in 1:N
        for ix in working_area
            lbl = mask[ix]

            for inb in 1:8
                nb_labels[inb] = mask[eightnbs[inb]+ix]
            end
            most_frequent_element_dict!(update_lbls, nb_labels, mfe_dict)
            if(isempty(update_lbls)) continue end  # skip if we don't have definite labeled NBs
            update_lbl = rand(update_lbls)
            if(update_lbl == lbl) continue end
            updates[update_lbl] = push!(get(updates, update_lbl, Vector{CartesianIndex{2}}()), ix)
        end
        # Execute the updates
        for (lbl, ixs) in updates
            mask[ixs] .= lbl
        end
        empty!(updates)
    end
    mask
end

struct SubImage
    left::Int
    right::Int
    top::Int
    bottom::Int
    center::Tuple{Int,Int}
    SubImage(left::Int, right::Int, top::Int, bottom::Int) = new(left, right, top, bottom, (left+div(right-left,2),bottom+div(bottom-top, 2)))
end

function applySubImage(sub::SubImage, img::Matrix{RGBA{Float32}})
    img[sub.top:sub.bottom,sub.left:sub.right]
end

function applySubImage(sub::SubImage, img::OffsetMatrix{RGBA{Float32}, Matrix{RGBA{Float32}}})
    img[sub.top:sub.bottom,sub.left:sub.right]
end

function cutSubimages(mask::OffsetMatrix{Int64, Matrix{Int64}}, colThresh::Int64)
    subImgs = []
    for lbl in 2:maximum(mask)
        coords = findall(l -> l == lbl, mask)
        minY = minimum(map(ix -> ix[1], coords))
        maxY = maximum(map(ix -> ix[1], coords))
        minX = minimum(map(ix -> ix[2], coords))
        maxX = maximum(map(ix -> ix[2], coords))

        push!(subImgs, SubImage(minX, maxX, minY, maxY))
    end
    sort!(subImgs, by=(si)->si.center)
    col = 1
    subImgsCol = [(first(subImgs), col)]
    for i in 2:length(subImgs)
        imgi = subImgs[i]
        imgidec = subImgs[i-1]
        col = subImgsCol[i-1][2]
        if abs(imgi.center[1] - imgidec.center[1]) > colThresh
            col += 1
        end
        push!(subImgsCol, (imgi, col))
    end

    sort!(subImgsCol, by=(sic)->(sic[2], sic[1].center[2]))

    ks = []
    vls = []
    for c in 1:col
        sics = map(sic -> sic[1], subImgsCol[map(sic -> sic[2], subImgsCol) .== c])
        push!(ks, Symbol(Char(c+64)))
        push!(vls, sics)
    end
    (; zip(ks, vls)...)
end
